package com.bytebonding.businesscards.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


import com.bytebonding.businesscards.R;
import com.bytebonding.businesscards.adapter.AdapterPerson;
import com.bytebonding.businesscards.entity.Person;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {


    String TAG = "B=B" + getClass();
    String jsonDataURL = "https://s3-us-west-2.amazonaws.com/udacity-mobile-interview/CardData.json";
    ArrayList personArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate.start");
        setContentView(R.layout.layout_activity_splash);

        int SPLASH_DISPLAY_LENGTH = 2000;

         personArrayList = new ArrayList();

        LoadData loadData = new LoadData();
        loadData.execute(jsonDataURL);






        Log.d(TAG,"oncreate.end");


    }


    class LoadData extends AsyncTask<String, Void, String> {
        String TAG = "" + getClass();


        @Override
        protected String doInBackground(String... params) {

            String jsonString = null;


            try {
                String urlString = params[0];
                HttpURLConnection urlConnection = null;
                URL url = new URL(urlString);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setReadTimeout(10000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.setDoOutput(true);
                urlConnection.connect();

                BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
                StringBuilder sb = new StringBuilder();

                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                jsonString = sb.toString();
                System.out.println("JSON: " + jsonString);
            } catch (IOException e) {

                Log.d(TAG, "Exception e=" + e.getMessage());
                e.printStackTrace();
            }

            return jsonString;
        }

        @Override
        protected void onPostExecute(String jsonString) {


            Log.d(TAG,"onPostExecute");


            Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);

            mainIntent.putExtra("PERSON_JSON_ARRAY_STRING",jsonString);
            SplashActivity.this.startActivity(mainIntent);
            Log.d(TAG," Redirect to MainActivity");
            SplashActivity.this.finish();

//            loading_progressBar.setVisibility(View.GONE);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}
