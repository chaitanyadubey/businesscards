package com.bytebonding.businesscards.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.bytebonding.businesscards.R;

import java.io.IOException;
import java.lang.reflect.Field;

public class OcrActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "B=B" + getClass();
    SurfaceView cameraSurfaceView, rectangleSurfaceView;
    TextView textView;
    CameraSource cameraSource;
    GridView gridView_new_ticket;
    ImageButton imageButton_rescan;
    Button button_use_new_ticket;
    SurfaceHolder surfaceHolderCamera, surfaceHolderRectangle;

    ImageView imageView_scanLine;

    final int RequestCameraPermissionID = 1001;

//    Ticket mNewTicket;


    Canvas canvas;
    Paint paint;

    Animation mAnimation;




    private boolean flashOn() {
        if (cameraSource == null) {
            return false;
        }

        Field[] declaredFields = null;
        try {
            declaredFields = CameraSource.class.getDeclaredFields();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (declaredFields == null) {
            return false;
        }

        for (Field field : declaredFields) {
            if (field.getType() == Camera.class) {
                field.setAccessible(true);
                try {
                    Camera camera = (Camera) field.get(cameraSource);
                    if (camera != null) {
                        Camera.Parameters params = camera.getParameters();
                        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                        camera.setParameters(params);
                        return true;
                    }

                    return false;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        return false;
    }





    public void scannerLineAnimationStart()
    {

        mAnimation = new TranslateAnimation(0, 0, 40, 200);


        mAnimation.setDuration(3000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setFillAfter(true);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        imageView_scanLine.setAnimation(mAnimation);

    }

    public void scannerLineAnimationStop()
    {
         mAnimation.cancel();

    }

    public void hideTicketUseButton(boolean flag)
    {
        RelativeLayout.LayoutParams layoutParamsButton = (RelativeLayout.LayoutParams) button_use_new_ticket.getLayoutParams();

        RelativeLayout.LayoutParams layoutParamsScanLine = (RelativeLayout.LayoutParams) imageView_scanLine.getLayoutParams();



        if(flag) {

            button_use_new_ticket.setVisibility(View.INVISIBLE);
            layoutParamsButton.height = 0;


            imageView_scanLine.setVisibility(View.VISIBLE);
            layoutParamsScanLine.height = layoutParamsScanLine.WRAP_CONTENT;

        }
        else {
            button_use_new_ticket.setVisibility(View.VISIBLE);
            layoutParamsButton.height = layoutParamsButton.WRAP_CONTENT;


            imageView_scanLine.setVisibility(View.INVISIBLE);
            layoutParamsScanLine.height = 0;
        }

        button_use_new_ticket.setLayoutParams(layoutParamsButton);
        imageView_scanLine.setLayoutParams(layoutParamsScanLine);

    }
    //==========================================onCreate==============================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate 1 ");
        setContentView(R.layout.layoutactivity_ocr);
        Log.d(TAG, "onCreate 2");

//        mNewTicket = new Ticket();

        cameraSurfaceView = (SurfaceView) findViewById(R.id.cameraSurfaceView);
        textView = (TextView) findViewById(R.id.textView);

        imageView_scanLine = (ImageView) findViewById(R.id.imageView_scanLine);

       // paint = new Paint();
       // Log.d(TAG, "onCreate 3");
// surfaceView.setOnTouchListener(this);



        surfaceHolderCamera = cameraSurfaceView.getHolder();

        surfaceHolderCamera.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        rectangleSurfaceView = (SurfaceView)findViewById(R.id.rectangleSurfaceView);
        surfaceHolderRectangle = rectangleSurfaceView.getHolder();
        surfaceHolderRectangle.setFormat(PixelFormat.TRANSPARENT);

        surfaceHolderRectangle.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);

        imageButton_rescan = (ImageButton)findViewById(R.id.imageButton_rescan);
        imageButton_rescan.setOnClickListener(this);


        button_use_new_ticket = (Button)findViewById(R.id.button_use_new_ticket);
        button_use_new_ticket.setOnClickListener(this);

        hideTicketUseButton(true);


//        gridView_new_ticket = (GridView)findViewById(R.id.gridView_new_ticket);
//        MyGridBaseAdapter myGridBaseAdapter = new MyGridBaseAdapter();
//        gridView_new_ticket.setAdapter(myGridBaseAdapter);


        scannerLineAnimationStart();

        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();


        if (!textRecognizer.isOperational())

        {
            Toast.makeText(this, "Not available ", Toast.LENGTH_SHORT).show();
        } else {

            cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(512, 512)
                    .setRequestedFps(8.0f)
                    .setAutoFocusEnabled(true)
                    .build();

            flashOn();

            cameraSurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder surfaceHolder) {

                    int widthMeasured=rectangleSurfaceView.getMeasuredWidth();

                    Log.d(TAG,"widthMeasured"+widthMeasured);
                    int heightMeasured=rectangleSurfaceView.getMeasuredHeight();

                    Log.d(TAG,"heightMeasured"+heightMeasured);

                    int width=rectangleSurfaceView.getMeasuredWidth();
                    Log.d(TAG,"width"+width);
                    int height=rectangleSurfaceView.getMeasuredHeight();
                    Log.d(TAG,"width"+height);

                    float RectLeft = rectangleSurfaceView.getLeft()+50;
                    float RectTop = rectangleSurfaceView.getTop()+50 ;
                    float RectRight = rectangleSurfaceView.getRight()-50 ;
                    float RectBottom = rectangleSurfaceView.getBottom() -50;


                    try {

                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {


                            ActivityCompat.requestPermissions(OcrActivity.this, new String[]{Manifest.permission.CAMERA}, RequestCameraPermissionID);
                            return;
                        }
                        cameraSource.start(cameraSurfaceView.getHolder());

                    } catch (IOException e)

                    {

                        e.printStackTrace();
                        Log.d(TAG, "IOException" + e.getMessage());


                    }

                }

                @Override
                public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

                    cameraSource.stop();

                }
            });

        }
        textRecognizer.setProcessor(new Detector.Processor<TextBlock>()
                                    {

                                        @Override
                                        public void receiveDetections(Detector.Detections<TextBlock> detections) {


                                            final SparseArray<TextBlock> items = detections.getDetectedItems();
                                            if (items.size() != 0)
                                            {
                                                textView.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        StringBuilder stringBuilder = new StringBuilder();
                                                        for (int i = 0; i < items.size(); i++)
                                                        {
                                                            TextBlock item = items.valueAt(i);
                                                            stringBuilder.append(item.getValue());
                                                            stringBuilder.append("\n");
                                                        }
                                                        String ocrData = stringBuilder.toString();
                                                        textView.setText(ocrData);

                                                        Log.d(TAG,"ocrData="+ocrData);



//                                                        mNewTicket.setOCRData(ocrData);

//                                                        if(mNewTicket.isHasDataChanged()) {
//                                                            MyGridBaseAdapter myGridBaseAdapter1 = (MyGridBaseAdapter) gridView_new_ticket.getAdapter();
//                                                            myGridBaseAdapter1.notifyDataSetChanged();
//
//                                                            if(mNewTicket.isTicketValid())
//                                                            {
//
//                                                                hideTicketUseButton(false);
//
//                                                                eraseGridOnSurfaceView();
//
//                                                                MainActivity.playSound(MainActivity.mScanSuccess,getApplicationContext());
//
//                                                                scannerLineAnimationStop();
//
//                                                            }
//                                                            else
//                                                            {
//                                                                hideTicketUseButton(true);
//
//
//                                                            }
//
//                                                        }



                                                    }
                                                });

                                            }
                                        }

                                        @Override
                                        public void release() {

                                        }
                                    }
        );


    }
    //==========================================onRequestPermissionsResult==============================================
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        switch (requestCode) {
            case RequestCameraPermissionID: {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)

                {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)

                    {
                        return;
                    }

                    try {
                        cameraSource.start(cameraSurfaceView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }


        }


    }

// @Override
// public boolean onTouch(View v, MotionEvent event) {
//
// float RectLeft = event.getX() - 100;
// float RectTop = event.getY() - 100 ;
// float RectRight = event.getX() + 100;
// float RectBottom = event.getY() + 100;
// DrawFocusRect(RectLeft , RectTop , RectRight , RectBottom , Color.BLUE);
// return true;
// }


//    public class MyGridBaseAdapter extends BaseAdapter
//    {
//        public String TAG = "B=B MyGridBaseAdapter";
//
//        public int ticketNumber;
//
//        @Override
//        public int getCount() {
//            return Ticket.getTotalCellCount();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return null;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return 0;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//
///* Logic : position is a single number , we have to break it into two parts column and row
//converting 1D array position to 2D row and column
//*/
//
//            if(convertView==null) {
//
//                LayoutInflater layoutInflater = getLayoutInflater();
//
//                convertView = getLayoutInflater().inflate(R.layout.ticket_cell_layout, null);
//            }
//
//
//            int row = (int) (position / Ticket.COLUMNS);
//            int column = position % Ticket.COLUMNS;
//
//
//            TextView textView_cell_value = (TextView)convertView.findViewById(R.id.textView_cell_value);
//
//            Cell cell = mNewTicket.getCell(row,column);
//
//
//            textView_cell_value.setText(cell.getValueWithHighestRank());
//            if(cell.isValueSet())
//            {
//                setBackground(convertView,R.drawable.cell_border2,R.color.ticket_bg_color2);
//            }
//            else
//            {
//                setBackground(convertView,R.drawable.cell_border1,R.color.ticket_bg_color1);
//            }
//
//            return convertView;
//        }//getView
//
//        public  void setBackground(View view,int drawableResourceId,int alternativeColorId )
//        {
//
//            Log.d(TAG,"setBackground");
//
//            try {
//                Method methodDef = view.getClass().getMethod("setBackground");
//                Log.d(TAG,"methodDef="+methodDef);
//                view.setBackground(getDrawable(drawableResourceId));
//            }
//            catch(NoSuchMethodException e)
//            {
//                Log.d(TAG,"NoSuchMethodException"+e.getMessage());
//
//                view.setBackgroundColor(getResources().getColor(alternativeColorId));
//            }
//            catch (Exception e)
//            {
//                Log.d(TAG,"Error setting setBackground "+e.getMessage());
//            }
//
//        }//setBackground
//
//
//
//
//    }//MyGridBaseAdapter


    @Override
    public void onClick(View v) {

        int sourceId = v.getId();

        if(sourceId == R.id.imageButton_rescan)
        {
//            mNewTicket = new Ticket();


            hideTicketUseButton(true);

//            drawGridOnSurfaceView(Color.RED);

//            MyGridBaseAdapter myGridBaseAdapter1 = (MyGridBaseAdapter) gridView_new_ticket.getAdapter();
//            myGridBaseAdapter1.notifyDataSetChanged();

            scannerLineAnimationStart();

        }
        else
        if(sourceId == R.id.button_use_new_ticket)
        {

            Intent intent = new Intent();
            setResult(RESULT_OK,intent);


//
//            Ticket ticket2 = new Ticket();
//
//            String row02[] = {"1",null,"26",null,"42","51",null,"72",null};
//            ticket2.setRow(0,row02);
//
//            String row12[] = {null,"12",null,"39",null,"55",null,"76","82"};
//            ticket2.setRow(1,row12);
//
//            String row22[] = {"8",null,"28",null,"45",null,"69",null,"87"};
//            ticket2.setRow(2,row22);
//
//            Log.d(TAG,"Ticket 2="+ticket2);
//


            Bundle bundle = new Bundle();
//            bundle.putParcelable("NEW_TICKET",mNewTicket);
            intent.putExtras(bundle);


            finish();
        }

    }


// public class Box extends View {
// private Paint paint = new Paint();
//
// Box(Context context) {
// super(context);
// }
// }
//
//
//
// public void onDraw(Canvas canvas) { // Override the onDraw() Method
//
// Paint paint=new Paint();
// paint.setStyle(Paint.Style.STROKE);
// paint.setColor(Color.GREEN);
// paint.setStrokeWidth(10);
//
// //center
// int x0 = canvas.getWidth()/2;
// int y0 = canvas.getHeight()/2;
// int dx = canvas.getHeight()/3;
// int dy = canvas.getHeight()/3;
// //draw guide box
// canvas.drawRect(x0-dx, y0-dy, x0+dx, y0+dy, paint);
// }
} //==========================================Activity End==============================================
