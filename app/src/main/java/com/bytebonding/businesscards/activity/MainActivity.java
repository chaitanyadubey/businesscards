package com.bytebonding.businesscards.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bytebonding.businesscards.R;
import com.bytebonding.businesscards.adapter.AdapterPerson;
import com.bytebonding.businesscards.entity.Person;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    String TAG = "B=B" + getClass();

    RecyclerView recyclerView;
    private static final int mINTERNET_PERMISSON = 1;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    ArrayList personArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Log.d(TAG, "onCreate");
        setContentView(R.layout.layout_activity_main);

        personArrayList = new ArrayList();

        Intent intent= getIntent();

        String personJsonArrayString = intent.getStringExtra("PERSON_JSON_ARRAY_STRING");



        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Scan", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, OcrActivity.class);
            startActivity(intent);
            }
        });

        setUpActionBarForDrawerLayout();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);


        if (!hasAllPermissions()) {
            Toast.makeText(this, "Does not have required permissions.", Toast.LENGTH_SHORT).show();
            return;
        }


        int numberOfColumns = 1;

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), numberOfColumns, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        AdapterPerson adapter = new AdapterPerson(getApplicationContext(), personArrayList);

        recyclerView.setAdapter(adapter);

        populatePersonList(personJsonArrayString);

    }

    public void populatePersonList(String personJsonArrayString)
    {

        Log.d(TAG," personJsonArrayString="+personJsonArrayString);

           AdapterPerson adapter = (AdapterPerson) recyclerView.getAdapter();

           adapter.clearList();

           ArrayList<Person> personArrayList = adapter.getPersonArrayList();



        try {
            JSONArray jsonArray = new JSONArray(personJsonArrayString);

            for (int i = 0, size = jsonArray.length(); i < size; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String firstName = jsonObject.getString("firstName");
                String lastName = jsonObject.getString("lastName");
                String email = jsonObject.getString("email");
                String company = jsonObject.getString("company");
                String startDate = jsonObject.getString("startDate");
//                    String bio = jsonObject.getString("bio");
                String avatar = jsonObject.getString("avatar");

                Person person = new Person();

                person.setFirstName(firstName);
                person.setLastName(lastName);
                person.setEmail(email);
                person.setCompany(company);
                person.setStartDate(startDate);
//                    person.setBio(bio);
                person.setAvatar(avatar);

                personArrayList.add(person);

            }

              recyclerView.setAdapter(adapter);


        } catch (JSONException e) {

            Log.d(TAG, "JSONException=" + e.getMessage());

            e.printStackTrace();
        }


    }

    public void setUpActionBarForDrawerLayout()
    {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this, /* host Activity */
                mDrawerLayout, /* DrawerLayout object */
                R.string.drawer_open, /* "open drawer" description */
                R.string.drawer_close /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.app_name);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.action_settings);
            }
        };
        mDrawerToggle.syncState();

// Set the drawer toggle as the DrawerListener
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
// Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    //======================hasAllPermissions====================
    public boolean hasAllPermissions()
    {
        boolean hasAllPermissions = true;
        int permissionCheck;
        permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            hasAllPermissions = false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, mINTERNET_PERMISSON);
        }
        Log.d(TAG,"hasAllPermissions="+hasAllPermissions);
        return hasAllPermissions;
    }

    //======================LoadData====================


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        mSearchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_menu_search));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
               // Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
               if(mDrawerLayout.isDrawerOpen(GravityCompat.START))
               {
                   mDrawerLayout.closeDrawer(GravityCompat.START);  // CLOSE DRAWER
               }else
               {
                   mDrawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
               }
                return true;

            case R.id.action_search:
                Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

            Toast.makeText(this, "camera", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_gallery) {

            Toast.makeText(this, "gallery", Toast.LENGTH_SHORT).show();


        } else if (id == R.id.nav_slideshow) {

            Toast.makeText(this, "slideshow", Toast.LENGTH_SHORT).show();


        } else if (id == R.id.nav_manage) {

            Toast.makeText(this, "manage", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_share) {

            Toast.makeText(this, "share", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_send) {

            Toast.makeText(this, "send", Toast.LENGTH_SHORT).show();

        }
        mDrawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }
}
